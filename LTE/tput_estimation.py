import math


class TputEstimation:
    def __init__(self, carrier_freq, enb_power, receiver_gain_db, transmitter_gain_dbi, bandwidth, symbols_per_time_slot):
        self.carrier_f_MHz = carrier_freq
        self.enb_power_dbm = enb_power
        self.receiver_gain_dB = receiver_gain_db
        self.transmitter_gain_dBi = transmitter_gain_dbi
        self.bandwidth = bandwidth
        self.vertical_pattern_gain = {'optimistic': {10: -2, 20: -7, 30: -19, 40: -30, 50:
                                                     -35, 60: -27, 70: -24, 80: -23, 90: -23},
                                      'pessimistic': {10: -38, 20: -37, 30: -37, 40: -35, 50:
                                                      -34, 60: -33, 70: -35, 80: -34, 90: -23}}
        self.horizontal_gain = -6
        self.noise_level_dBm = -120
        self.resource_blocks = self.bandwidth * 5
        self.number_of_subcarriers = 12
        self.bits_to_megabits = pow(1024, 2)
        self.milliseconds_in_second = 1000
        self.symbols_per_time_slot = symbols_per_time_slot
        self.symbols_per_subframe = self.symbols_per_time_slot * 2
        self.parameters = {'optimistic': {'eirp': {}, 'FSL': {}, 'SNR': {}, 'capacity': {}},
                           'pessimistic': {'eirp': {}, 'FSL': {}, 'SNR': {}, 'capacity': {}}}

        self.snr_to_cqi = {1.95: 1, 4.0: 2, 6.0: 3, 8.0: 4, 10.0: 5, 11.95:
                           6, 14.05: 7, 16.00: 8, 17.9: 9, 19.9: 10, 21.5:
                           11, 23.45: 12, 25.00: 13, 27.3: 14, 29.0: 15}

        self.cqi_to_mcs = {1: [4, 78 / 1024], 2: [4, 120 / 1024], 3: [4, 193 / 1024], 4: [4, 308 / 1024],
                           5: [4, 449 / 1024], 6: [4, 602 / 1024], 7: [16, 378 / 1024], 8: [16, 490 / 1024],
                           9: [16, 616 / 1024], 10: [64, 466 / 1024], 11: [64, 567 / 1024], 12: [64, 666 / 1024],
                           13: [64, 772 / 1024], 14: [64, 873 / 1024], 15: [64, 948 / 1024]}

        self.alt_60_degree = tuple(range(50, 2001, 50))
        self.received_power = {}

        self.fulfill_parameters()
        self.signal_worst_angle()

    def snr_to_mcs(self, var):
        try:
            for stc in self.snr_to_cqi:
                if var < stc:
                    mod = self.cqi_to_mcs[self.snr_to_cqi[stc]][0]
                    coding = self.cqi_to_mcs[self.snr_to_cqi[stc]][1]
                    break
                else:
                    mod = self.cqi_to_mcs[len(self.cqi_to_mcs)][0]
                    coding = self.cqi_to_mcs[len(self.cqi_to_mcs)][1]
            return mod, coding
        except:
            print("Snr_to_cqi probably empty")

    def fulfill_parameters(self):
        for version in self.vertical_pattern_gain:
            pattern = self.vertical_pattern_gain[version]
            for elevation in pattern:
                self.parameters[version]['eirp'][elevation] = pattern[elevation] + self.horizontal_gain \
                                                              + self.enb_power_dbm + self.transmitter_gain_dBi
                self.parameters[version]['FSL'][elevation] = 32.44 \
                                                           + 20 * math.log10(1 / math.sin(math.radians(elevation))) \
                                                           + 20 * math.log10(self.carrier_f_MHz)
                self.parameters[version]['SNR'][elevation] = self.parameters[version]['eirp'][elevation] - \
                                                             self.parameters[version]['FSL'][elevation] + \
                                                             self.receiver_gain_dB - self.noise_level_dBm

                [modulation, coding_rate] = self.snr_to_mcs(self.parameters[version]['SNR'][elevation])

                self.parameters[version]['capacity'][elevation] = math.log2(modulation) * self.number_of_subcarriers \
                                                                  * self.resource_blocks * self.symbols_per_subframe \
                                                                  * self.milliseconds_in_second * coding_rate \
                                                                  / self.bits_to_megabits

    def signal_worst_angle(self):
        for a60 in self.alt_60_degree:
            path_loss = 32.44 + 20 * math.log10(a60 / math.cos(math.radians(60))) + 20 * math.log10(self.carrier_f_MHz)
            self.received_power[a60] = self.parameters['pessimistic']['eirp'][60] - path_loss + self.receiver_gain_dB
