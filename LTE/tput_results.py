from LTE.tput_estimation import TputEstimation
import matplotlib.pyplot as plt


class TputResults:
    def __init__(self):
        self.frequencies = [1.4, 3, 5, 10, 15, 20]
        self.estimated_capacity = []
        self.tput_est_objects = []
        self.tput_estim()
        self.draw_charts()

    def tput_estim(self):
        for freq in self.frequencies:
            self.tput_est_objects.append(TputEstimation(1800, 43, 12, 11.3, freq, 6))

        for obj in self.tput_est_objects:
            self.estimated_capacity.append(min(obj.parameters['optimistic']['capacity'].values()))

    def draw_charts(self):
        plt.subplot(221)
        snr = sorted(self.tput_est_objects[0].parameters['optimistic']['SNR'].items())
        x, y = zip(*snr)
        plt.plot(x, y)
        plt.xlabel("angle [degree]")
        plt.ylabel("SNR [dB]")
        plt.title("SNR at 1 km")

        plt.subplot(222)
        throughput = sorted(self.tput_est_objects[0].parameters['optimistic']['capacity'].items())
        x, y = zip(*throughput)
        plt.plot(x, y)
        plt.xlabel("antena angle [degree]")
        plt.ylabel("Throughput [Mbit/s]")
        plt.title("Throughput at 1 km for 1.4MHz")

        plt.subplot(223)
        throughput = sorted(self.tput_est_objects[5].parameters['optimistic']['capacity'].items())
        x, y = zip(*throughput)
        plt.plot(x, y)
        plt.xlabel("antena angle [degree]")
        plt.ylabel("Throughput [Mbit/s]")
        plt.title("Throughput at 1 km for 20MHz")

        plt.subplot(224)
        plt.bar(self.estimated_capacity, height=[capacity for capacity in self.estimated_capacity])
        plt.xticks(self.estimated_capacity, self.frequencies)
        plt.xlabel("bandwidth [MHz]")
        plt.ylabel("Throughput [Mbit/s]")
        plt.title("Throughput per channel bandwidth")
        plt.show()


tput_results = TputResults()
